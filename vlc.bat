@echo off
setlocal

REM 设置日志文件的路径和文件名
set "log_file=.\bat_log.txt"

REM 获取当前批处理文件所在的目录路径
for %%i in ("%~dp0.") do set "current_dir=%%~fi"

REM 构建 main.conf 文件的完整路径
set "config_file=%current_dir%\main.conf"
REM 读取 main.conf 文件的内容
set /p vlc_root=<"%config_file%"

REM 构建 url.conf 文件的完整路径
set "url_config_file=%current_dir%\url.conf"
REM 读取 url.conf 文件的内容
set /p url=<"%url_config_file%"

REM 清空日志文件内容（如果需要），或注释掉以下行
echo. > "%log_file%"

REM 将输出重定向到日志文件
echo Running the script... >> "%log_file%"

REM 执行 VLC 命令
echo Executing VLC command... >> "%log_file%"
call "%vlc_root%" "%url%"  --play-and-exit >> "%log_file%"
echo url:%url% >> "%log_file%"

REM 脚本结束时，输出结束信息到日志文件并关闭日志
echo Script completed. >> "%log_file%"
echo. >> "%log_file%"

endlocal