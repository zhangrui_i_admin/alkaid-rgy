/*
 * @Author: zhangrui BruceZhang@huayun.com
 * @Date: 2023-07-18 14:28:31
 * @LastEditors: ZhangRui 673229632@qq.com
 * @LastEditTime: 2023-07-19 22:06:12
 * @FilePath: \alkaid-rgy\script\vbs.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package script

import (
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"

	"golang.org/x/text/encoding/simplifiedchinese"
)

func ReadMainConfig(configFilePath string) string {
	content, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		log.Fatal("Error reading file:", err)
	}
	return string(content)
}

func SaveMainConfig(configFilePath string, content string) error {
	err := ioutil.WriteFile(configFilePath, []byte(content), 0644)
	if err != nil {
		log.Fatal("Error writing file:", err)
		return err
	}
	return nil
}

// 执行vbs
func ExecuteVBSScript(scriptPath string, url string) error {

	if url == "" {
		return fmt.Errorf("url地址为空")
	}

	// 保存url地址
	SaveMainConfig("./url.conf", url)

	cmd := exec.Command("cscript", scriptPath)
	out, err := cmd.Output()
	if err != nil {
		log.Println("执行VBS脚本失败:", err)
		return err
	}

	output, _ := simplifiedchinese.GBK.NewDecoder().Bytes(out)
	fmt.Printf("%s\n", output)
	return nil
}
