/*
 * @Author: ZhangRui 673229632@qq.com
 * @Date: 2023-07-17 22:45:15
 * @LastEditors: ZhangRui 673229632@qq.com
 * @LastEditTime: 2023-07-19 22:03:40
 * @FilePath: \Alkaid RGY\main.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package main

import (
	"alkaid-rgy/launch"
	"alkaid-rgy/script"
	"alkaid-rgy/websocket"
	"log"
	"os"
	"strings"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/driver/desktop"
	"fyne.io/fyne/v2/storage"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"github.com/flopp/go-findfont"
)

var window fyne.Window

const configFilePath = "./main.conf"
const urlConfigFilePath = "./url.conf"

func init() {
	//设置中文字体:解决中文乱码问题
	fontPaths := findfont.List()
	for _, path := range fontPaths {
		if strings.Contains(path, "msyh.ttf") || strings.Contains(path, "simhei.ttf") ||
			strings.Contains(path, "simsun.ttc") || strings.Contains(path, "simkai.ttf") {
			os.Setenv("FYNE_FONT", path)
			break
		}
	}

}

func message(message string, w fyne.Window) {
	dialog.ShowInformation("提示", message, w)
}

func makeTray(a fyne.App) {
	if desk, ok := a.(desktop.App); ok {
		// h := fyne.NewMenuItem("配置", func() {
		// 	window.Show()
		// })
		// h.Icon = theme.HomeIcon()
		menu := fyne.NewMenu("菜单")
		desk.SetSystemTrayMenu(menu)

		res, _ := fyne.LoadResourceFromPath("./icon.png")
		desk.SetSystemTrayIcon(res)
	}
}

func logLifecycle(a fyne.App) {
	a.Lifecycle().SetOnStarted(func() {
		log.Println("Lifecycle: Started")
	})
	a.Lifecycle().SetOnStopped(func() {
		log.Println("Lifecycle: Stopped")
		websocket.CloseWebSocket()
	})
	a.Lifecycle().SetOnEnteredForeground(func() {
		log.Println("Lifecycle: Entered Foreground")
	})
	a.Lifecycle().SetOnExitedForeground(func() {
		log.Println("Lifecycle: Exited Foreground")
	})
}

func main() {
	res, _ := fyne.LoadResourceFromPath("./icon.png")

	myApp := app.NewWithID("io.alkaid.rgy")

	myApp.SetIcon(res)
	makeTray(myApp)

	logLifecycle(myApp)
	window = myApp.NewWindow("Alkaid RGY")
	window.Resize(fyne.NewSize(700, 400))
	myApp.Settings().SetTheme(theme.DarkTheme())

	// 开启监听
	websocket.OpenSocket()

	launchEntry := widget.NewCheck("开机自启动", func(b bool) {

		if b {
			launch.OpenLaunch()
		} else {
			launch.CloseLaunch()
		}
	})
	launchEntry.Checked = launch.CheckLaunch()

	vlcAddressEntry := widget.NewEntry()
	vlcAddressEntry.SetText(script.ReadMainConfig(configFilePath))

	chooseVbsButton := widget.NewButton("选择路径", func() {

		fd := dialog.NewFileOpen(func(reader fyne.URIReadCloser, err error) {
			if err != nil {
				dialog.ShowError(err, window)
				return
			}
			if reader == nil {
				log.Println("Cancelled")
				return
			}

			filePath := strings.Replace(reader.URI().String(), "file://", "", 1)
			log.Println("Choose to...", filePath)
			vlcAddressEntry.SetText(filePath)
		}, window)
		fd.SetFilter(storage.NewExtensionFileFilter([]string{".exe"}))
		fd.Show()
	})

	saveVbsButton := widget.NewButton("保存", func() {
		if vlcAddressEntry.Text == "" {
			message("请选择安装路径", window)
			return
		}
		script.SaveMainConfig(configFilePath, vlcAddressEntry.Text)
		message("保存成功", window)
	})

	vbsEntry := widget.NewEntry()
	vbsEntry.SetText("http://localhost:999/video/demo.ts")

	runVbsButton := widget.NewButton("开始测试", func() {
		err := script.ExecuteVBSScript("./start.vbs", vbsEntry.Text)
		if err != nil {
			log.Println("执行Vbs失败:", err)
		} else {
			log.Println("执行Vbs已完成")
		}
	})

	// 创建布局
	content := container.NewVBox(
		// launchEntry,
		container.NewGridWithRows(1,
			widget.NewLabel("请选择vlc安装路径"),
		),
		container.NewBorder(nil, nil, nil, chooseVbsButton, vlcAddressEntry),

		saveVbsButton,
		container.NewGridWithRows(2,
			widget.NewLabel("测试视频地址"),
			vbsEntry,
		),
		runVbsButton,
	)

	window.SetContent(content)
	window.CenterOnScreen() // 将窗口居中显示

	window.ShowAndRun()

}
